export const state = () => ({
    navigation: [], config: []
})

export const mutations = {
    setNavigation(state, navigation) {
        state.navigation = navigation;
    }, setConfig(state, config) {
        state.config = config;
    },
}

export const actions = {

    setKeitaro() {
        const links = document.querySelectorAll('a');
        // const urlParts = ['5zaz', 'qq62', 's2yp', 'pret1', 'pret2', 'pret3', 'fortune', 'winoui', 'machance-casino-avis', 'lucky8-casino-avis', 'casino-joka-avis', 'vegasplus-casino-avis', 'vegas', 'tortuga', 'madnix', 'top', 'machance', 'unique', 'truefortuna', 'monte', 'block', 'cresus', 'luckyluke', 'top2', 'top3', 'wildsultan', 'jack21', 'azurrr', 'banzai', 'gratowin', 'joka', 'tortuga-bonus', 'azurrr-bonus', 'banzai-bonus', 'wildsultan-bonus', 'cresus-bonus', 'lucky8-bonus', 'madnix-bonus', 'winoui-bonus', 'vegasplus-bonus', 'machance-bonus', 'unique-bonus', 'monte-bonus', 'jack21-bonus', 'luckyluke-bonus', 'joka-bonus', 'extra', 'intense', 'arlequin', 'stop-war', 'top1-modal', 'black-label', 'vegas-neoserf', 'madnix-neoserf', 'intense-neoserf', 'unique-neoserf', 'azur-neoserf', 'cresus-top', 'vegas-top', 'madnix-top', 'tortuga-top', 'play-zax'];
        const urlParts = ['keitaro-replace-img'];
        urlParts.forEach(part => {
            for (let link of links) {
                let path = link.pathname.split('/');
                let id = link.getAttribute('id');
                path.forEach(url => {
                    if (url.length >= 1) {
                        let match = RegExp(`^${part}$`, 'gi');
                        if (match.test(url)) {
                            if (!id) link.setAttribute('id', part);
                            link.setAttribute('href', 'https://realink.org/Jf2jYsYn')
                            link.setAttribute('target', '_blank')
                        }
                    }
                });
            }
        })

        // let queryString;
        // let linkString;
        // const keitaroUrl = 'realink.org';
        // const queryParams = ['sub_id_1', 'sub_id_2']
        // for (let link of links) {
        //     let href = link.getAttribute('href');
        //     let matchKeitaro = RegExp(keitaroUrl, 'g');
        //     if (matchKeitaro.test(href)) {
        //         // link.addEventListener('click', (e) => {
        //         //     e.preventDefault();
        //             let query = queryParams.map(param => {
        //                 let params = [param];
        //                 switch (param) {
        //                     case 'sub_id_1':
        //                         params[1] = link.getAttribute('id');
        //                         break;
        //                     case 'sub_id_2':
        //                         params[1] = window.location.href;
        //                         break;
        //                     case 'sub_id_3':
        //                         params[1] = `${link.getAttribute('id')}|${window.location.href}`;
        //                         break;
        //                     case 'sub_id_4':
        //                         params[1] = link.innerHTML;
        //                         break;
        //                 }
        //                 return params.join('=');
        //             });
        //             queryString = query.join('&');
        //             // window.location.href = `${href}?${queryString}`;
        //             link.setAttribute('href', `${href}?${queryString}`)
        //         // });
        //     }
        // }

        let queryString;
        const keitaroUrl = 'realink.org';
        const queryParams = ['sub_id_1', 'sub_id_2']
        for (let link of links) {
            let href = link.getAttribute('href');
            let matchKeitaro = RegExp(keitaroUrl, 'g');
            if (matchKeitaro.test(href)) {
                link.removeAttribute('href');
                link.addEventListener('click', (e) => {
                    e.preventDefault();
                    let query = queryParams.map(param => {
                        let params = [param];
                        switch (param) {
                            case 'sub_id_1':
                                params[1] = link.getAttribute('id');
                                break;
                            case 'sub_id_2':
                                params[1] = window.location.href;
                                break;
                            case 'sub_id_3':
                                params[1] = `${link.getAttribute('id')}|${window.location.href}`;
                                break;
                            case 'sub_id_4':
                                params[1] = link.innerHTML;
                                break;
                        }
                        return params.join('=');
                    });
                    queryString = query.join('&');
                    window.location.href = `${href}?${queryString}`;
                });
            }
        }
    },

    async fetchNavigation({ commit }) {
        const response = await this.$strapi.find('navigation', {
            populate: ['items', 'items.dropdown']
        })
        await commit('setNavigation', response.data);
    },

    async fetchConfig({ commit }) {
        const response = await this.$strapi.find('config')
        await commit('setConfig', response.data);
    },

    async nuxtServerInit({ dispatch }) {
        dispatch('fetchNavigation');
        dispatch('fetchConfig');
    }
}


export const getters = {
    navigation: state => state.navigation.items, config: state => state.config,
}
