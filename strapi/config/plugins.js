module.exports = ({ env }) => ({
    'transformer': {
        enabled: true,
        config: {
            prefix: '/api/',
            responseTransforms: {
                removeAttributesKey: true,
                removeDataKey: true,
            }
        }
    },
    upload: {
        config: {
            providerOptions: {
                sizeLimit: 250 * 1024 * 1024
            }
        }
    },
    'editorjs': {
        enabled: true
    },
    'seo': {
        enabled: true
    },
    'navigation': {
        enabled: false
    },
    "record-locking": {
        enabled: false,
    },
    "content-versioning": {
        enabled: false,
    },
    'preview-button': {
        enabled: false,
        config: {
            contentTypes: [
                {
                    uid: 'api::publication.publication',
                    targetField: 'slug',
                    published: {
                        basePath: 'preview',
                    },
                },
            ],
        },
    },
    'test': {
        enabled: false,
        resolve: './src/plugins/test'
    },
});
