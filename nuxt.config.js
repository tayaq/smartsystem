export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'Smartsystem',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        ],
        bodyAttrs: {
            class: 'page'
        }
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '@/assets/css/style.min.css'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        // { src: '~/plugins/keitaro.js', mode: 'client', ssr: false }
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recomme nded): https://go.nuxtjs.dev/config-modules
    buildModules: ['@nuxtjs/dotenv'],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: ['@nuxtjs/strapi', '@nuxtjs/gtm',],
    gtm: {
        id: 'GTM-KN6VPCB',
        enabled: true,
        autoInit: true
    },
    strapi: {
        url: process.env.STRAPI_URL || `http://localhost:1337/api`,
        // entities: ['index-page', 'review']
    },
    axios: {
        baseURL: process.env.STRAPI_URL || `http://localhost:1337/api`,
    }
}
