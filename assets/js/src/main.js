const faqQuestions = document.querySelectorAll('.faq__question, .faq__switcher');
const switcher = document.querySelector('.nav__switcher');

for (let question of faqQuestions) {
    question.addEventListener('click', () => {
        question.parentElement.parentElement.classList.toggle('faq__item--active');
    });
}

switcher.addEventListener('click', () => {
    switcher.parentElement.classList.toggle('nav--open');
});

const contentTitles = document.querySelectorAll('.article__content h3');
const anchorsList = document.querySelector('.anchors__list');

for (let title of contentTitles) {
    addAnchorItem(title.innerText, anchorsList);
}

const anchorsItems = document.querySelectorAll('.anchors__item');

for (let item of anchorsItems) {
    item.addEventListener('click', (e) => {
        for (let title of contentTitles) {
            if (title.innerHTML === item.innerHTML) {
                const y = title.getBoundingClientRect().top + window.scrollY - 20;
                window.scroll({
                    top: y,
                    behavior: 'smooth'
                });
                break;
            }
        }
    });
}

function addAnchorItem(text, list) {
    let item = document.createElement('li');
    item.classList.add('anchors__item');
    let linkText = document.createTextNode(text);
    item.appendChild(linkText);
    anchorsList.append(item);
}
